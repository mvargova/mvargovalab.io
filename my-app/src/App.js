
import './App.css';
import {
  HorizontalLayout,
  VerticalLayout,
  Panel,
  Separator,
 // Spacer,
  View
} from "nice-react-layout";

function App() {
  return (
    <div className="App">
      <View>
      <HorizontalLayout mockup>
  <Panel proportion = {7}>
    <VerticalLayout mockup>
      <Panel>
      <HorizontalLayout mockup>
      <Panel/>
      <Separator/>
      <Panel/>
    </HorizontalLayout>
    </Panel>
      <Separator/>
      <Panel>
        
      <VerticalLayout mockup>
      <Panel proportion = {3}>
        <button className = "click1" onClick/>
        <button className = "click2" onClick/>
        <button className = "click3" onClick/>
        <button className = "click4" onClick/>
        <button className = "click5" onClick/>
        <button className = "click6" onClick/>
        <button className = "click7" onClick/>
        <button className = "click8" onClick/>
        <button className = "click9" onClick/>
        <button className = "click10" onClick/>
        <button className = "click11" onClick/>
        <button className = "click12" onClick/>
        <button className = "click13" onClick/>
        <button className = "click14" onClick/>
        <button className = "click15" onClick/>
        <button className = "click16" onClick/>
        <button className = "click17" onClick/>
        <button className = "click18" onClick/>
        <button className = "click19" onClick/>
        <button className = "click20" onClick/>
        <button className = "click21" onClick/>
        <button className = "click22" onClick/>
        <button className = "click23" onClick/>
        <button className = "click24" onClick/>
        </Panel>
      <Separator/>
      <Panel proportion = {2}>
      <button className = "click1" onClick/>
      <button className = "click2" onClick/>
      <button className = "click4" onClick/>
      <button className = "click5" onClick/>
      <button className = "click7" onClick/>
      <button className = "click8" onClick/>
      <button className = "click10" onClick/>
      <button className = "click11" onClick/>
      <button className = "click13" onClick/>
      <button className = "click14" onClick/>
      <button className = "click16" onClick/>
      <button className = "click17" onClick/>
      <button className = "click19" onClick/>
      <button className = "click20" onClick/>
      <button className = "click22" onClick/>
      <button className = "click23" onClick/>
        </Panel>
    </VerticalLayout>
    </Panel>
    </VerticalLayout>
    </Panel>
  <Separator />
  <Panel proportion = {2}>
  <button className = "click1" onClick/>
    </Panel>
    </HorizontalLayout>
      </View>
    </div>
  );
}

export default App;
