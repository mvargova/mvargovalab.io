import React, { useState, useEffect } from "react";

const Counter = ({ num = 94 }) => {
  const [n, setN] = useState(parseInt(num));
  const [col, setCol] = useState("black");
  //const [size, setSize] = useState(n);

  /* useEffect(() => {
    console.log("counter is mounted");
    return () => {
      console.log("counter is unmounted");
    };
  }, []); */

  useEffect(() => {
    if (n >= 50) {
      setCol("red");
    } else {
      if (n >= 40) {
        setCol("pink");
      } else {
        if (n >= 30) {
          setCol("blue");
        } else {
          if (n >= 20) {
            setCol("green");
          } else {
            if (n >= 10) {
              setCol("orange");
            } else {
              setCol("black");
            }
          }
        }
      }
    }
    //console.log(n);
  }, [n]);

  /*  useEffect(() => {
    if (size <= 10) {
      setSize(10);
    } else {
      setSize(n);
    }
  }, [n]); */
  return (
    <div>
      <p>Pripočítať/odpočítať 1</p>
      <p style={{ color: col, fontSize: n }}>{n}</p>
      <button onClick={() => setN(n + 1)}>+</button>
      <button onClick={() => setN(n - 1)}>-</button>
    </div>
  );
};
export default Counter;
