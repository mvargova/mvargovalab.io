import React, { useState } from "react";
import Counter from "./Counter";

const CounterManager = ({ numberOfCounters = 3 }) => {
  const [n, setN] = useState(parseInt(numberOfCounters));

  /* useEffect(() => {
    console.log("counterManager is mounted");
    return () => {
      console.log("counterManager is unmounted");
    };
  }, []); */

  /*useEffect(() => {
    console.log(`number of counter is ${n}`);
  }, [n]);*/
  const counters = () => {
    return [...Array(n)].map((_, i) => {
      return <Counter key={i} num={(i + 1) * 10} />;
    });
  };

  return (
    <div>
      <p>Zvýšiť/znížiť počet počítadiel</p>
      <button onClick={() => setN(n + 1)}>+</button>
      <button onClick={() => setN(n - 1)}>-</button>
      {counters()}
    </div>
  );
};
export default CounterManager;
