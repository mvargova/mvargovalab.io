import React from "react";
import Gallery from "react-photo-gallery";
import { photos } from "./Photo.js";

const Family = () => {
  return (
    <div>
      <h1>Moji najbližší</h1>
      <p>Teraz Vám ukážem ľudí, ktorých mám na tomto svete najradšej!</p>
      <Gallery photos={photos} />
    </div>
  );
};

export default Family;
