import "./App.css";

import React from "react";
import {
  HashRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import Home from "./Home";
import Education from "./Education";
import Contact from "./Contact";
import Work from "./Work";
import Hobbies from "./Hobbies";
import Family from "./Family";
import Menu from "./Menu";
import CounterManager from "./CounterManager";
import User from "./User";

const App = () => {
  return (
    <Router>
      <div>
        <Menu />
        <Switch>
          <Route path="/education">
            <Education />
          </Route>
          <Route path="/contact">
            <Contact />
          </Route>
          <Route path="/work">
            <Work />
          </Route>
          <Route path="/hobbies">
            <Hobbies />
          </Route>
          <Route path="/family">
            <Family />
          </Route>
          <Route path="/home">
            <Home />
          </Route>
          <Route path="/user">
            <User />
          </Route>
          <Route path="/counter">
            <CounterManager numberOfCounters="2" />
          </Route>
          <Redirect from="/" to="/home" />
        </Switch>
      </div>
    </Router>
  );
};
export default App;
