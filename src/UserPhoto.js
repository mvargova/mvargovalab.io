import React from "react";

const UserPhoto = ({ user }) => {
  return (
    <div>
      <img alt={user.name} src={user.avatar_url} />
      <p>{user.name}</p>
    </div>
  );
};
export default UserPhoto;
