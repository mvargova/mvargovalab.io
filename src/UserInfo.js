import React from "react";

const UserInfo = ({ user }) => {
  return (
    <div>
      <p className="info">{user.name}</p>
      <p className="info">{user.web_url}</p>
      <p className="info">{user.id}</p>
      <p className="info">{user.username}</p>
      <p className="info">{user.state}</p>
    </div>
  );
};
export default UserInfo;
