import React from "react";

const Contact = () => {
  return (
    <div>
      <h1>Kontakt</h1>
      <p> Mária Vargová </p>
      <p> E-mail: mariavargova@student.upjs.sk </p>
      <p> E-mail (súkromný): maria.vargova96@gmail.com</p>
      <p> Škola: UPJŠ Prírodovedecká fakulta </p>
    </div>
  );
};
export default Contact;
