export const photos = [
  {
    src: "./images/r1.jpg",
    width: 3,
    height: 2,
  },
  {
    src: "./images/r2.jpg",
    width: 3,
    height: 2,
  },
  {
    src: "./images/r3.jpg",
    width: 3,
    height: 2,
  },
  {
    src: "./images/r4.jpg",
    width: 3,
    height: 2,
  },
  {
    src: "./images/r5.jpg",
    width: 2,
    height: 3,
  },
  {
    src: "./images/r6.jpg",
    width: 3,
    height: 2,
  },
  {
    src: "./images/r7.jpg",
    width: 3,
    height: 2,
  },
];
