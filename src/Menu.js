import React from "react";
import "./App.css";
import { NavLink } from "react-router-dom";
import logo from "./logo.jpg";

const Menu = () => {
  return (
    <div>
      <img className="logo" src={logo} alt="logo" />
      <ul>
        <li>
          <NavLink to="/home">O mne</NavLink>
        </li>
        <li>
          <NavLink to="/family">Moji najbližší</NavLink>
        </li>
        <li>
          <NavLink to="/education">Vzdelanie</NavLink>
        </li>
        <li>
          <NavLink to="/work">Pracovné skúsenosti</NavLink>
        </li>
        <li>
          <NavLink to="/hobbies">Záľuby</NavLink>
        </li>
        <li>
          <NavLink to="/contact">Kontakt</NavLink>
        </li>
        <li>
          <NavLink to="/counter">:)</NavLink>
        </li>
        <li>
          <NavLink to="/user">:P</NavLink>
        </li>
      </ul>
    </div>
  );
};
export default Menu;
