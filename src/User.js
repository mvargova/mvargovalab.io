import React, { useEffect, useState } from "react";
import Flippy, { FrontSide, BackSide } from "react-flippy";
import UserInfo from "./UserInfo";
import UserPhoto from "./UserPhoto";

const User = ({ userName = "mvargova" }) => {
  const [data, setData] = useState({ error: null, isLoaded: false, users: [] });
  useEffect(() => {
    fetch(`https://gitlab.com/api/v4/users/?username=${userName}`)
      .then((res) => res.json())
      .then(
        (result) => {
          setData({
            isLoaded: true,
            users: result,
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          setData({
            isLoaded: true,
            error,
          });
        }
      );
  }, [userName]);

  const { error, isLoaded, users } = data;
  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  } else if (users.length === 0) {
    return <div>{userName} was not found.</div>;
  } else {
    return (
      <div class="flippystyle">
        <Flippy
          flipOnHover={true} // default false
          flipOnClick={false} // default false
          flipDirection="horizontal" // horizontal or vertical
          //ref={(r) => (this.flippy = r)} // to use toggle method like this.flippy.toggle()
          // if you pass isFlipped prop component will be controlled component.
          // and other props, which will go to div
          style={{ width: "200px", height: "200px" }} /// these are optional style, it is not necessary
        >
          <FrontSide
            style={{
              backgroundColor: "#07b7dd",
            }}
          >
            <UserPhoto user={users[0]} />
          </FrontSide>
          <BackSide style={{ backgroundColor: "#07b7dd" }}>
            <UserInfo user={users[0]} />
          </BackSide>
        </Flippy>
      </div>
    );
  }
};
export default User;
