//import logo from "./logo.svg";
//import "./App.css";

import React from "react";

const Education = () => {
  return (
    <div>
      <h1>Vzdelanie</h1>
      <p>
        2017 – 2021 -- Prírodovedecká fakulta UPJŠ, Šrobárová 2, 041 54 Košice
      </p>
      <p>2007 – 2015 -- Gymnázium AP, Komenského 40, 083 01 Sabinov</p>
      <p>2003 – 2007 -- Základná škola, Uzovce 160, 082 66</p>
    </div>
  );
};
export default Education;
