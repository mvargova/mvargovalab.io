import React from "react";
import ja1 from "./ja1.jpg";
import ja2 from "./ja2.jpg";

const Home = () => {
  return (
    <div>
      <h1>O mne</h1>
      <p>Ahojte! Vítam vás na mojej stránke! :)</p>
      <p>Na tejto stránke sa dozviete najzaujímavejšie veci z môjho života.</p>
      <p>Na začiatok si môžte pozrieť mňa ako dieťa, a mňa momentálne.</p>
      <img className="ja1" src={ja1} alt="ja1" />
      <img className="ja2" src={ja2} alt="ja2" />
    </div>
  );
};
export default Home;
