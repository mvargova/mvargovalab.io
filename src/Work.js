//import logo from "./logo.svg";
//import "./App.css";

import React from "react";

const Work = () => {
  return (
    <div>
      <h1>Pracovné skúsenosti</h1>
      <h1 className="podnazov">PRAX</h1>
      <p>
        05/2016 -- Absolventská prax - ÚPSVaR Sabinov – ADMINISTRATÍVNA PRÁCA{" "}
      </p>

      <h1 className="podnazov">PRACOVNÉ SKÚSENOSTI</h1>
      <p>
        10/2018 – 12/2020 -- Ride Pub, Drienica 553, 083 01 Sabinov, BARMANKA -
        ČAŠNÍČKA
      </p>
      <p>
        04/2018 – 10/2018 -- Šariš Park, Železničná 1900, 082 21 Veľký Šariš,
        RECEPČNÁ - BARMANKA
      </p>
      <p>
        06/2016 – 09/2017 -- VÚB banka, a. s. Nám. Slobody 90, 083 01 Sabinov,
        PORADCA KLIENTA
      </p>
      <p>10/2016 - 12/2016 -- Orange Slovensko, a. s., PROMOTÉRKA</p>
      <p>
        06/2015 – 08/2015 -- CEMM THOME SK, spol. s.r.o., Budovateľská 40, 080
        01 Prešov, OPERÁTORKA VÝROBY
      </p>
    </div>
  );
};
export default Work;
