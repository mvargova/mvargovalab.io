//import logo from "./logo.svg";
//import "./App.css";

import React from "react";
import zaluba1 from "./zaluba1.jpg";
import zaluba2 from "./zaluba2.jpg";
import zaluba3 from "./zaluba3.jpg";

const Hobbies = () => {
  return (
    <div>
      <h1>Záľuby</h1>
      <p>A teraz prišiel čas na moje záľuby!</p>
      <p>Momentálne je mojou najväčšou záľubou inline korčuľovanie.</p>
      <p>Šoférovanie, to je zase niečo pri čom si viem vyvetrať hlavu.</p>
      <p>A turistika, to je niečo čo ma nadchýňa. Tie pohľady z hora sú TOP!</p>
      <img className="zaluba1" src={zaluba1} alt="zaluba1" />
      <img className="zaluba2" src={zaluba2} alt="zaluba2" />
      <img className="zaluba3" src={zaluba3} alt="zaluba3" />
    </div>
  );
};
export default Hobbies;
